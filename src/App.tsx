import Subcribers from "./pages/Subcribers"
function App() {

  return (
    <div className="flex justify-center flex-col">
      <Subcribers />
    </div>
  )
}

export default App
