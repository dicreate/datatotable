import { TableContainer, Tbody, Td, Th, Thead, Tr, Table, Spinner, Text } from "@chakra-ui/react";
import axios from "axios"
import { useEffect, useState } from "react"

interface ISubscribers {
   id: string;
   name: string;
   number: string;
   organization?: string;
   address: string;
   startDate: string;
   endDate: string;
}

interface ITableProps {
   phone: string;
   isLoading: boolean;
   setIsLoading: (isLoading: boolean) => void;
}

const TableData: React.FC<ITableProps> = ({ phone, isLoading, setIsLoading }) => {

   const [subscribers, setSubscribers] = useState<ISubscribers[]>();
   const [isError, setIsError] = useState(false);

   useEffect(() => {
      const fetchSubscribers = async () => {
         try {
            setIsError(false);
            setIsLoading(true);
            const res = await axios.get(`https://6681464704acc3545a063a57.mockapi.io/${phone}`);
            const data = res.data;

            if (res.statusText === "OK") {
               setSubscribers(data)
               return;
            } else {
               setIsError(true);
               return;
            }
         } catch (error) {
            if (error instanceof Error) {
               console.log(error)
               setIsError(true);
            }
         } finally {
            setIsLoading(false);
         }
      }
      fetchSubscribers();
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [phone])

   if (isLoading) return (
      <div className="flex justify-center items-center">
         <Spinner
            thickness='4px'
            speed='0.65s'
            emptyColor='gray.200'
            color='blue.500'
            size='xl'
         />
      </div>
   )

   return (
      !isError
         ? <TableContainer>
            <Table variant={"striped"} size={'sm'}>
               <Thead>
                  <Tr>
                     <Th>Имя</Th>
                     <Th>Номер телефона</Th>
                     <Th>Организация</Th>
                     <Th>Адрес Регистрации</Th>
                     <Th>Дата начала</Th>
                     <Th>Дата окончания</Th>
                  </Tr>
               </Thead>
               <Tbody>
                  {
                     subscribers?.map((subscriber) => (
                        <Tr key={subscriber.id}>
                           <Td>{subscriber.name}</Td>
                           <Td>{subscriber.number}</Td>
                           <Td>{subscriber.organization}</Td>
                           <Td>{subscriber.address}</Td>
                           <Td>{subscriber.startDate}</Td>
                           <Td>{subscriber.endDate}</Td>
                        </Tr>
                     ))
                  }
               </Tbody>
            </Table>
         </TableContainer>
         : <Text fontSize='24px' color='tomato'>Такого номера нет в базе данных !</Text>
   )
}

export default TableData