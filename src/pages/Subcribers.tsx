import { Button, Input, InputGroup, InputLeftElement, FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/react'
import TableData from '../components/Table'
import { PhoneIcon } from '@chakra-ui/icons'
import { useState, useRef } from 'react';
const Subcribers = () => {
   const [phone, setPhone] = useState('');
   const [isLoading, setIsLoading] = useState(false);
   const [isError, setIsError] = useState(false);
   const inputRef = useRef<HTMLInputElement>(null);

   function handleValidateClick() {
      if (inputRef.current && inputRef.current?.value) {
         setPhone(inputRef.current.value);
         setIsError(false);
      } else {
         setIsError(true);
         setPhone('');
      }
   }

   return (
      <div className='flex flex-col items-center gap-20'>
         <FormControl isInvalid={isError} display={'flex'} flexDirection={"column"} gap={"10px"} borderWidth='1px' borderRadius='lg' mt="10" p="4" maxW={"480px"} w={"100%"}>
            <FormLabel as='legend'>Мобильные номера</FormLabel>
            <InputGroup>
               <InputLeftElement pointerEvents='none'>
                  <PhoneIcon color='gray.300' />
               </InputLeftElement>
               <Input ref={inputRef} type='tel' placeholder='Введите номер телефона ...' />
            </InputGroup>
            {isError ? <FormErrorMessage>Поле номер телефона не должно быть пустым !</FormErrorMessage> : null}
            <Button onClick={handleValidateClick} isLoading={isLoading} maxW={'200px'} w={"100%"} alignSelf={"end"} colorScheme='cyan'>Отправить</Button>
         </FormControl>
         {phone ? <TableData phone={phone} isLoading={isLoading} setIsLoading={setIsLoading} /> : null}
      </div>
   )
}

export default Subcribers